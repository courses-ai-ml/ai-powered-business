# AI Powered Business
> © Horia Mărgărit 2021. All Rights Reserved.

This repository accompanies and is tightly integrated with [my online course](https://bit.ly/3CCVtdu). It is _highly_ recommended to [enroll in my course](https://bit.ly/3CCVtdu) to best understand *_what_* each of the notebooks does, *_where_* to apply the algorithms and methods implemented within the notebooks, and *_why_* a business cares about the results obtained using these notebooks.

## Running The Course Notebooks
After you have successfully completed this particular section, your web browser should look something like this

![GIF example of a running Jupyter notebook server](https://jupyter-notebook.readthedocs.io/en/latest/_images/new-notebook.gif)

To attain this *_zen_* state, you must first successfully complete the the *_requirements_* section below. Then you can run the course notebooks by executing the following five commands in your favorite terminal, and navigating in your web browser to one of the two URLs provided by the `Jupyter` notebook server.

```zsh
mkdir -p ~/courses/ai-ml # only run once; skip all subsequent times
git clone git@gitlab.com/courses-ai-ml/ai-powered-business.git . # only run once; skip all subsequent times
# if the line above fails, then try `git clone https://gitlab.com/courses-ai-ml/ai-powered-business.git .`
conda activate ai-powered-business
jupyter notebook --autoreload --no-browser --notebook-dir=`pwd`/ai-powered-business/notebooks
conda deactivate # only after stopping the jupyter notebook server
```

The _backtick_ enclosed `pwd` is a sub-command within your terminal that is required above. It ensures the `Jupyter` notebook server reads the course notebooks which were _cloned_ from this repository (using `git`) into your local computer's directory at `<current working directory>/ai-powered-business/notebooks`.

Executing the five commands above causes your terminal to reply with a series of messages that look very similar to the following example output

```zsh
[I H:m:s.ms NotebookApp] Autoreload enabled: the webapp will restart when any Python src file changes.
[I H:m:s.ms NotebookApp] Serving notebooks from local directory: <current working directory>/ai-powered-business/notebooks
[I H:m:s.ms NotebookApp] Jupyter Notebook ?.?.? is running at:
[I H:m:s.ms NotebookApp] http://localhost:8888/?token=<cryptographic string that looks like gibberish>
[I H:m:s.ms NotebookApp]  or http://127.0.0.1:8888/?token=<cryptographic string that looks like gibberish>
[I H:m:s.ms NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C H:m:s.ms NotebookApp] 
    
    To access the notebook, open this file in a browser:
        file://<some file path that you should AVOID; instead use one of the URLs below>
    Or copy and paste one of these URLs:
        http://localhost:8888/?token=<cryptographic string that looks like gibberish>
     or http://127.0.0.1:8888/?token=<cryptographic string that looks like gibberish>
```

The two most important take-aways from this example output are that
- Control-C stops this server (and pressing it twice skips confirmation)
- Opening your favorite web browser and navigating to either of the two provided URLs enables you to *_view, edit and run_* the course notebooks

## Requirements
Multiple scientific computing toolkits are required to run the provided `Jupyter` course notebooks. It is _highly_ recommended to use the `miniconda` _package manager_ for installing and configuring these toolkits. For example, installing `miniconda` and executing the following command in your favorite terminal

```zsh
conda create -n ai-powered-business \
             -c conda-forge \
             jupyter \
             keras \
             lightgbm \
             matplotlib \
             pandas \
             python=3.8 \
             scikit-learn \
             scipy \
             shap \
             skccm \
             statsmodels \
             tensorflow-mkl \
             -yq
```

should cause your computer to create a siloed compute environement named `ai-powered-business` into which the package manager will _automatically_ download, install and configure the required scientific computing toolkits.

To be able to use the toolkits, you must _activate_ the siloed compute environment by executing the following command in your favorite terminal

```zsh
conda activate ai-powered-business
```

which you can _deactivate_ at any time with the command

```zsh
conda deactivate
```

and you can *_permanently delete_* any siloed compute environement, by name, with the following command

```zsh
conda remove -n ai-powered-business --all -yq
```

which will result in your computer's _state_ being as if you had never created or used the siloed compute environement in the first place.

### Installing Miniconda

The best way to install `miniconda` is to download their executable installer from [their website here](https://docs.conda.io/en/latest/miniconda.html#latest-miniconda-installer-links). Subsequently running their executable installer will provide your favorite terminal with the `conda` command used throughout this *_readme_*.

### Toolkits
You can also find all the reference documentation on each of the used toolkits at their respective websites, linked below. It is _recommended_ to study the specific portions within these toolkits that are used throughout the course notebooks.

- [jupyter](https://jupyter-notebook.readthedocs.io/en/stable/)
- [keras](https://keras.io/api/)
- [lightgbm](https://lightgbm.readthedocs.io/en/latest/Python-API.html)
- [matplotlib](https://matplotlib.org/stable/api/index.html)
- [miniconda](https://docs.conda.io/en/latest/miniconda.html)
- [pandas](https://pandas.pydata.org/pandas-docs/stable/reference/index.html)
- [python-3.x](https://docs.python.org/3/library/index.html)
- [scikit-learn](https://scikit-learn.org/stable/modules/classes.html)
- [scipy](https://docs.scipy.org/doc/scipy/reference/reference/index.html)
- [shap](https://shap.readthedocs.io/en/latest/)
- [skccm](https://skccm.readthedocs.io/en/latest/)
- [statsmodels](https://www.statsmodels.org/stable/api.html)
- [tensorflow-mkl](https://www.tensorflow.org/api_docs/python/tf)
